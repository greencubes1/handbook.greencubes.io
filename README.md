
![Build Status](https://gitlab.com/bamhm182/mdbook/badges/master/pipeline.svg)

---

https://greencubes1.gitlab.io/handbook.greencubes.io

---

Example [mdBook][] website using GitLab Pages.

## GitLab CI

This project's static Pages are built by [GitLab CI][ci], following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml):

```yaml
# requiring the environment of Rust 1.42.0
image: rust:1.42.0

before_script:
  - wget -qO- https://github.com/rust-lang/mdBook/releases/download/v0.3.7/mdbook-v0.3.7-x86_64-unknown-linux-gnu.tar.gz | tar -xvz -C /usr/local/bin
  - chmod 0755 /usr/local/bin/mdbook
    
# the 'pages' job will build your site and deploy if on master
pages:
  stage: deploy
  script:
    - mdbook build $([ "$CI_COMMIT_BRANCH" = "master" ] && echo "-d public") # build to public path on master
  artifacts:
    paths:
      - public
      - book
    expire_in: 1 day
```

## Building locally

To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project
1. [Install mdBook][install]
1. Preview your project: `mdbook serve`
1. Add content
1. Generate the website: `mdbook build` (optional)
1. Push your changes to the master branch: `git push`

Read more at mdbook's [documentation][].

