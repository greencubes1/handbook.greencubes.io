# Provisioned Resources

The top level manifest for most everything below:
[gcb-k8s-resources](https://gitlab.com/greencubes1/gcb-k8s-resources)

## Digital Ocean Kubernetes cluster:

### Machines
Two nodes.. 4GB RAM 2vcpu each

### Services
Current:
* nginx server for [React frontend](components/2dView.md)
* infinilapse::frame-compiler
* [infinilapse::dslr-webcam-manager](https://gitlab.com/infinilapse/infinilapse)

Planned, not yet developed:
* [abberationMonitor](https://gitlab.com/greencubes1/aberrationMonitor)


Peviously used, not active
* KubeEdge control plane
* openBalena server


## On Premise Cluster

### Machines
* System76 (32GB RAM, 12 CPU)
* RaspberryPis
    * waves (pumphouse mycodo host)
        * pumphouse.ngrok.io (gcb:greencubes)
    * blackfin (infinilapse host)
    * red (planti mycodo host)
        * planti.ngrok.io (gcb:greencubes)
* BeagleBonBlack


### Services:
* [nginx server for React frontend](components/2dView.md)
* [infinicomp](https://gitlab.com/infinilapse/infinilapse)   :: system76
* [mqtt solenoid driver](git@gitlab.com:greencubes1/python_mqtt_responder.git) :: BeagleBone
* [Mycodo](https://github.com/kizniche/Mycodo) :: RPi
    * Note: Working on a kubified deploy of Mycodo .. Volumes need populated

