# Outstanding Tasks

Need to do:
* Provision Mycodo via k8s
    * Mycodo uses a lot of volumes which are bootstrapped by the host OS (via a normal install).  This should instead be properly bootstrapped within docker (no bind mounts).


Maybe do:
* Our own openBalena server.