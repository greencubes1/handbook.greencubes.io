# A farmer's journey 🌿 - the Book! 📖

Hello you've discovered "the Book" for GreenCubes.


# Who is this book for?

This book was most immediately crafted for new owners of a GreenCube farm.  This is a community resource which is co-authored by all of us to help new farmers integrate into their local farming scene.

 [GNU Free Documentation License](https://www.gnu.org/licenses/fdl-1.3.en.html)
